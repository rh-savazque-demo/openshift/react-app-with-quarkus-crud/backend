package org.acme;

import java.util.List;
import java.util.stream.Collectors;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.acme.Student;
import jakarta.ws.rs.QueryParam;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.PathParam;


import jakarta.enterprise.context.ApplicationScoped;

@Path("/student")
@ApplicationScoped
public class StudentResource {
    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Student> getAll() throws Exception {
        return Student.findAll().list();
    }

    @POST
    @Transactional
    public Response create(Student p) {
        if (p.name == null || p.email == null) return Response.ok().status(400).build();
        Student student = new Student();
        student.name = p.name;
        student.email = p.email;
        Student.persist(student);
        return Response.ok(p).status(200).build();
    }

    @PUT
    @Transactional
    @Path("/{id}")
    public Student update(@PathParam(value = "id") Long id, Student p) {
        Student entity = Student.findById(id);
        if (entity == null) Response.ok().status(400).build();

        if(p.name != null )  entity.name = p.name;
        if(p.email != null)    entity.email = p.email;

        return entity;
    }


    @DELETE
    @Path("/{id}")
    @Transactional
    public Response delete(@PathParam(value = "id") Long id) {
        Student entity = Student.findById(id);
        if (entity == null) Response.ok().status(400).build();
        entity.delete();
        return Response.status(204).build();
    }

}