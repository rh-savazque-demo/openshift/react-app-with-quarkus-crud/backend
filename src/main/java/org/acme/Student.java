package org.acme;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
public class Student extends PanacheEntity {
	@Column(name="name")
    public String name;

	@Column(name="email")
    public String email;

}